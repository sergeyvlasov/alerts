1. --remove README.rdoc
2. --add README.md
3. --GitHub/BitBucket repos: 10:52
4. --Heroku (gems, puma, app): 12:30
4.1 foreman, Procfile, .env: 01:04 
4.2 push to Heroku: 01:10
5. Welcome#index
6. RSpec, Capybara, Cucumber
6.1 Welcome#dashboard
6.2 Devise + Omniauth signup
7. Spring/Guard, rake-test, ...
8. Next Feature: View Alerts
8.1 Twitter Bootstrap 
9. Next Feature: Angular.js
10. Next Features: Add Alert, Send Alert, etc.
11. Additional clients
12. Analytics/Monitoring/Error handling(Heroku addons)
13. Alternative storage (MongoDB)
14. Alternative interfaces (command line, JSON API, etc.)
15. Alternative Deployment (Amazon EC2, Digital Ocean): requires DevOps work